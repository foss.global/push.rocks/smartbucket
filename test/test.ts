import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';

import * as smartbucket from '../ts/index.js';

const testQenv = new Qenv('./', './.nogit/');

let testSmartbucket: smartbucket.SmartBucket;
let myBucket: smartbucket.Bucket;
let baseDirectory: smartbucket.Directory;

tap.test('should create a valid smartbucket', async () => {
  testSmartbucket = new smartbucket.SmartBucket({
    accessKey: testQenv.getEnvVarOnDemand('S3_KEY'),
    accessSecret: testQenv.getEnvVarOnDemand('S3_SECRET'),
    endpoint: 's3.eu-central-1.wasabisys.com',
  });
});

tap.skip.test('should create testbucket', async () => {
  // await testSmartbucket.createBucket('testzone');
});

tap.skip.test('should remove testbucket', async () => {
  // await testSmartbucket.removeBucket('testzone');
});

tap.test('should get a bucket', async () => {
  myBucket = await testSmartbucket.getBucketByName('testzone');
  expect(myBucket).toBeInstanceOf(smartbucket.Bucket);
  expect(myBucket.name).toEqual('testzone');
});

// Fast operations
tap.test('should store data in bucket fast', async () => {
  await myBucket.fastStore('hithere/socool.txt', 'hi there!');
});

tap.test('should get data in bucket', async () => {
  const fileString = await myBucket.fastGet('hithere/socool.txt');
  const fileStringStream = await myBucket.fastGetStream('hithere/socool.txt');
  console.log(fileString);
});

tap.test('should delete data in bucket', async () => {
  await myBucket.fastRemove('hithere/socool.txt');
});

// fs operations

tap.test('prepare for directory style tests', async () => {
  await myBucket.fastStore('dir1/file1.txt', 'dir1/file1.txt content');
  await myBucket.fastStore('dir1/file2.txt', 'dir1/file2.txt content');
  await myBucket.fastStore('dir2/file1.txt', 'dir2/file1.txt content');
  await myBucket.fastStore('dir3/file1.txt', 'dir3/file1.txt content');
  await myBucket.fastStore('dir3/dir4/file1.txt', 'dir3/dir4/file1.txt content');
  await myBucket.fastStore('file1.txt', 'file1 content');
});

tap.test('should get base directory', async () => {
  baseDirectory = await myBucket.getBaseDirectory();
  const directories = await baseDirectory.listDirectories();
  console.log('Found the following directories:');
  console.log(directories);
  expect(directories.length).toEqual(3);
  const files = await baseDirectory.listFiles();
  console.log('Found the following files:');
  console.log(files);
  expect(files.length).toEqual(1);
});

tap.test('should correctly build paths for sub directories', async () => {
  const dir4 = await baseDirectory.getSubDirectoryByName('dir3/dir4');
  expect(dir4).toBeInstanceOf(smartbucket.Directory);
  const dir4BasePath = dir4.getBasePath();
  console.log(dir4BasePath);
});

tap.test('clean up directory style tests', async () => {
  await myBucket.fastRemove('dir1/file1.txt');
  await myBucket.fastRemove('dir1/file2.txt');
  await myBucket.fastRemove('dir2/file1.txt');
  await myBucket.fastRemove('dir3/file1.txt');
  await myBucket.fastRemove('dir3/dir4/file1.txt');
  await myBucket.fastRemove('file1.txt');
});

tap.start();
