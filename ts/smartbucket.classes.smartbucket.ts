import * as plugins from './smartbucket.plugins.js';
import { Bucket } from './smartbucket.classes.bucket.js';

export interface ISmartBucketConfig {
  endpoint: string;
  port?: number;
  useSsl?: boolean;
  accessKey: string;
  accessSecret: string;
}

export class SmartBucket {
  public config: ISmartBucketConfig;

  public minioClient: plugins.minio.Client;

  /**
   * the constructor of SmartBucket
   */
  constructor(configArg: ISmartBucketConfig) {
    this.config = configArg;
    this.minioClient = new plugins.minio.Client({
      endPoint: this.config.endpoint,
      port: configArg.port || 443,
      useSSL: configArg.useSsl !== undefined ? configArg.useSsl : true,
      accessKey: this.config.accessKey,
      secretKey: this.config.accessSecret,
    });
  }

  public async createBucket(bucketNameArg: string) {
    const bucket = await Bucket.createBucketByName(this, bucketNameArg);
    return bucket;
  }

  public async removeBucket(bucketName: string) {
    await Bucket.removeBucketByName(this, bucketName);
  }

  public async getBucketByName(bucketName: string) {
    return Bucket.getBucketByName(this, bucketName);
  }
}
